const items = require("./3-arrays-vitamins.cjs");
const obj = items.reduce((acc,item) => {
    const arr = item.contains.split(",");
    arr.map((vita) => {
        vita = vita.trim(" ");
        acc[vita] = [];
    })
    return acc
},{})

items.map((item) => {
    const name = item["name"];
    const arr = item.contains.split(",");
    arr.map((vita) => {
        let vitamin = vita.trim(" ");
        obj[vitamin].push(name);
    })
})

console.log(obj);
